# -*- coding: utf-8
#
# Generador de polinomios aproximadores al conjunto de datos, por ahora no es un modulo, se debe modificar para ajustar las necesidades del usuario :(
#
# author: Daniel J. Ramirez

import numpy as np
from utility import *
from dataminer import *
import traceback as tb


import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from scipy.stats import *
import copy

# data, ns = getData('results/latest/raw/', '3-1 50-5 80-10 100-50 200-100 1000')
data, ns = getData('results/E_k/', '3-1 50-5 80-10 100')



def get_ns_for_k(k):
    return list(ns[ns.index(int(max(3, k * 2))):])


def create_equation(k, alfa, local_ns, x_transform, poly_deg=3):
    k = k - 1
    values = []

    # Obtiene todos los valores criticos para un alfa
    for n in local_ns:
        try:
            values.append(float(data[alfa][str(n)][0][k]))
        except:
            import traceback as tb
            print ("n,k",n, k)
            tb.print_exc()
    x_sample_size = x_transform(np.array(local_ns))
    y_means = np.array(values)

    # polynomial interpolation
    polydata = np.polyfit(x_sample_size, y_means, poly_deg, full=True)

    poly = np.poly1d(polydata[0])

    # calcula r cuadrado
    yfit = poly(x_sample_size) + polydata[1]
    residuals = y_means - yfit
    SSresiduals = sum(pow(residuals, 2))
    SSTotal = len(y_means) * np.var(y_means)
    r2 = 1 - SSresiduals / SSTotal

    return poly, r2

functions = [
      lambda x: x
    , lambda x: np.log(x)
    , lambda x: np.log(np.log(x))
    , lambda x: np.log(np.log(np.log(x)))
    # , lambda x: np.log(np.log(np.log(np.log(x))))
    # , lambda x: np.log(np.log(np.log(np.log(x))))
    ]
functions_names = [
      'x'
    , 'ln(x)'
    , 'ln(ln(x))'
    , 'ln(ln(ln(x)))'
    # , 'ln(ln(ln(ln(x))))'
    # , 'x / 2'

]


def hard_test(alfa, k, x_transforms, poly_degrees):
    k = k - 1
    best_degree = poly_degrees[0]
    best_function = x_transforms[0]
    best_approved = 0
    best_fit = float('inf')
    for poly_deg in poly_degrees:
        for x_transform in x_transforms:
            approved = True
            approved_count = 0

            # obtiene una ecuacion de interpolacion en la cual se descarta un valor de n
            local_ns = get_ns_for_k(k + 1)
            for n in local_ns:
                mod_ns = copy.copy(local_ns)
                mod_ns.remove(n)
                try:
                    eq, r2 = create_equation(k+1, alfa, mod_ns, x_transform, poly_deg)
                except:
                    continue

                # obtenemos el valor simulado para n
                sim_value = float(data[alfa][str(n)][0][k])
                aprox_value = eq(x_transform(n))
                # obtenemos la incertidumbre
                i = float(data[alfa][str(n)][1][k])
                difference = abs(sim_value - aprox_value)
                # print(difference)
                # checamos si el valor aproximado esta en el intervalo de confianza
                if aprox_value >= sim_value - i and sim_value <= sim_value + i:
                    approved_count += 1
                approved &= difference < i
            if r2 < best_fit:
                best_approved = approved_count
                best_degree = poly_deg
                best_function = x_transform
            # if approved_count > best_approved:
            #     best_approved = approved_count
            #     best_degree = poly_deg
            #     best_function = x_transform

            # print(r2, difference, difference < i)
            # print(approved)
            # print()
    # print('alpha:',alfa)
    # print('approved:',best_approved, 'of', len(ns))
    # print('best degree',best_degree)
    # print('best function', functions_names[x_transforms.index(best_function)])
    # print()
    return best_degree, best_function


# eq, r2 = create_equation(1, ALFAS[-1], ns, lambda x: np.log(x), 8)
# print(eq, r2)
#
# print(eq(np.log(50)))
# print()

# plt.plot(ns, get_means(data, ns, ALFAS[0], 0), 'o')
# plt.plot(sample_size, np.polyval(polydata[0], np.log(sample_size)), 'r-')
# plt.ylabel('Medias')
# plt.xlabel('N')
# plt.show()


def latex_formater(eq, f_name):
    latex_str = "\\begin{equation}\n"
    # latex_str += "\\begin{split}\n"
    i = 0
    exp = len(eq)
    for term in eq:
        if term > 0 and i > 0:
            latex_str += '+'
        latex_str += '%s' % term
        if exp > 1:
            latex_str += '\ %s^{%s}\ ' % (f_name, exp)
        elif exp == 1:
            latex_str += '\ %s\ ' % (f_name)
        exp -= 1
        i += 1
    latex_str += '\n'
    # latex_str += "\\end{split}\n"
    latex_str += "\\end{equation}"

    return latex_str


for k in range(1, 5):
    alfa = '0.100000'
    deg, function = hard_test(alfa, k, functions, [6,7,8,9,10,11,12,13])
    function_name = functions_names[functions.index(function)]
    eq, r2 = create_equation(k, alfa, get_ns_for_k(k), function, deg)

    sim, = plt.plot(get_ns_for_k(k), get_means(data, get_ns_for_k(k), alfa, k - 1), 'o', label="Valores críticos simulados")
    regression, = plt.plot(get_ns_for_k(k), np.polyval(eq, function(get_ns_for_k(k))), 'r-', label="Ecuación de interpolación")
    plt.legend(handles=[sim, regression], loc=4)
    plt.ylabel('Medias')
    plt.xlabel('N')
    # fig = plt.figure()
    pp = PdfPages('alfa-%s_k-%s.pdf' % (alfa, k))
    pp.savefig()
    pp.close()
    plt.clf()

    print (r2)
    print(latex_formater(eq, function_name))
exit()
