# -*- coding: utf-8
#
# Este programa facilita la obtencion de datos crudos generados por el programa de generacion de valores criticos.
#
# author: Daniel J. Ramirez

import sys
import glob
import os
from decimal import Decimal as D
from utility import get_ns


base_subdirs = ['', 'error/', 'error_estandar/']


# DATA contiene todos los valores crudos en forma de cadena, para la media
# el error estandar y la incertidumbre
#
# DATA esta organizado de la siguiente forma
#
# DATA[alfa][N][tipo][k]
#
# donde alfa es el nivel de significancia,
# N es el tamaño de la muestra
# tipo es un indice en [0,1,2], que representa el tipo de dato, 0 indica que
# el arreglo en esa posicion contiene las medias, el 1 indica que el arreglo en
# esa posicion contiene las incertidumbres, y el 2 indica que el arreglo en
# en esa posicion contiene los errores estandares.
#
# Por ejemplo si quisieramos obtener el valor critico, la incertidumbre y el
# error estandar para algun alfa y algun N y algun valor de K obtendriamos
#
# media = DATA[alfa][N][0][K]
# incertidumbre = DATA[alfa][N][1][K]
# error_estandar = DATA[alfa][N][2][K]
# DATA = {}
ALFAS = [
      '0.900000' # 10%
    , '0.800000' # 20%
    , '0.700000' # 30%
    , '0.600000' # 40%
    , '0.500000' # 50%
    , '0.400000' # 60%
    , '0.300000' # 70%
    , '0.200000' # 80%
    , '0.100000' # 90%
    , '0.050000' # 95%
    , '0.020000' # 98%
    , '0.025000' # 97.5%
    , '0.010000' # 99%
    , '0.005000' # 99.5%
    , '0.002000' # 99.8%
    , '0.001000' # 99.9%
]

# prefijos de los nombres de los archivos para medias, incertidumbres y
# errores estandar respectivamente.
prefixes = ['criticos', 'errores', 'se']


def getData(base_dir, ns_string=""):
    if base_dir[-1] != '/':
        base_dir += '/'
    if not os.path.isdir(base_dir):
        return None
    ns = get_ns(ns_string or "3-1 50-5 80-10 100")
    DATA = {}
    # Obtiene los datos crudos y los almacena en una estructura conveniente para su
    # manipulacion
    for alfa in ALFAS:
        DATA[alfa] = {} # en este nuevo mapa agregaremos todos los valores
                        # crudos asociados con el alfa especificdado por la llave

        for n in ns:
            if not os.path.isdir(base_dir + str(n)):
                #print('no existe el directorio %s%s' % (base_dir, n))
                continue
            # este arreglo almacenara las medias, incertidumbres y errores estandar
            # para n y todos los k's posibles para dicho n.
            DATA[alfa][str(n)] = []
            i = 0 # indica que prefijo y subdirectorio debemos usar
            for prefix in prefixes:
                file_name = '%s_N_%s-alfa_%s.txt' % (prefix, n, alfa)
                file_name = "%s%s/%s%s" % (base_dir, n, base_subdirs[i], file_name)

                if not os.path.exists(file_name):
                    continue

                f = open(file_name, 'r')
                values = f.readlines()[0].split(',')
                values.pop() # debido al formato del archivo quedara un dato basura
                DATA[alfa][str(n)].append(values)
                i += 1
    return DATA, ns


def get_means(data, ns, alfa, k):
    values = []
    for n in ns:
        try:
            values.append(float(data[alfa][str(n)][0][k]))
        except:
            pass
    return values
