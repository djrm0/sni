    #
#
#

import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages


from utility import *
from dataminer import *

data, ns = getData('results/E_k/')

values = []
for n in ns:
    try:
        values.append(float(data['0.001000'][str(n)][2][0]))
    except:
        pass

size_dif = len(ns) - len(values)
ns = ns[size_dif:]
plt.plot(ns, values, 'bo-')
plt.ylabel('Error estandar')
plt.xlabel('N')
pp = PdfPages('confidence.pdf')
pp.savefig()
pp.close()
plt.clf()
