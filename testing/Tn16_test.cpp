
#include <random>
#include <iostream>

#include "../src/tests/Tn16.hpp"
#include "../src/element_t.hpp"
#include "../src/StatisticalTestSampler.hpp"



int main() {
  Tn16Ek ek;
  Tn16LkLower lk_lower;
  Tn16LkUpper lk_upper;

  double values[] = {-1.40, -.44, -.30, -.24, -.22, -.13, -.05, .06, .10, .18, .20, .39, .48, .63, 1.01};
  std::vector<element_t> v (values, values + sizeof(values) / sizeof(double) );

  // usando el metodo en la clase Tn16Ek
  double c = ek.test(v.size(), 2, &v); //    (1)
  std::cout << c << std::endl;

  // creando un sampler con la clase Tn16Ek
  std::seed_seq seed1 = {2383, 1193};
  std::seed_seq seed2 = {1234, 1231};
  // std::cout << typeid(seed1) << std::endl;

  StatisticalTestSampler sampler (&ek, seed1, seed2, 2);
  c = sampler.test(v.size(), 2, &v);  // hace los mismo que (1)
  std::cout << c << std::endl;

  // aplicacion de la prueba a una muestra aleatoria de 20
  c = sampler.test_from_random_sample(20, 2);
  std::cout << c << std::endl;
  // aplicacion de la prueba a una muestra aleatoria de 50
  c = sampler.test_from_random_sample(50, 2);
  std::cout << c << std::endl;


  std::cout << "----------------------" << std::endl;
  std::cout << "Test: Lk lower" << std::endl;
  // marcar alguna valor de v como contaminado
  double values2[] = {-.44, -.30, -.24, -.22, -.13, -.05, .06, .10, .18, .20, .39, .48, .63, 1.01};
  v = std::vector<element_t> (values2, values2 + sizeof(values2) / sizeof(double) );
  v[1].contamined = true;
  // usando la prueba con precheck activado, dado que el valor contaminado esta en la posicion 1 y estamos probando los valores superiores entonce precheck debe rechazar y retornar un valor enorme para que se rechaze el resultado de la prueba
  c = lk_upper.test(v.size(), 1, &v, true);
  std::cout << "lk_upper " << c << " (debe ser un valor GRANDE)" << std::endl;

  // crea un almacenamiento de resultados para pruebas con contaminacion
  CriticalComparatorResult ccr;
  // asignamos el valor critico objetivo para Lk n = 20, k = 2, alfa = .05
  ccr.set_target(.484);
  // creamos un sampler para Lk_lower, por default las semillas del generado alterno son seed2 y seed1
  StatisticalTestSampler sampler_lk_lo (&lk_upper, seed1, seed2, 2);
  // asignamos el almacenamiento de resultados
  sampler_lk_lo.test_results = &ccr;
  // ejecutamos la prueba Lk lower para n = 20, k = 2, para una muestra aleatoria con 2 valores contaminados con delta = 0.0
  for (int i = 0; i < 100000; i++) {
    double lk_c = sampler_lk_lo.test_from_random_contamined_sample(20, 2, 100);
    // std::cout << "critico n=20, k=2: " << lk_c << std::endl;
  }
  std::cout << "contador " << ccr.get_counter() << std::endl;

  std::cout << "FIN Test: Lk lower" << std::endl;
  std::cout << "----------------------" << std::endl;



  // aplicacion de la prueba a una muestra aleatoria de 20 repitiendo 100000 veces
  unsigned obs = 100000;
  std::vector<double> *criticals = sampler.repeat_test(20, 2, obs);
  std::sort(criticals->begin(), criticals->end());
  unsigned index = (unsigned)(.05 * obs);
  double critical = criticals->at(index);
  std::cout << critical << std::endl;
  delete criticals;

  return 1;
}
