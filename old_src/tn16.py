#
#
#
#
import random
import numpy
import math


def ek(n, k, data):
    """ Realiza una prueba estadistica Ek (tietjen-moore(1979)) """

    mean = numpy.mean(data)
    # calculamos los n residuales absolutos
    # r = map(lambda e : abs(e - mean), data)
    r = [abs(e - mean) for e in data]
    # ahora obtenemos un ordenamiento indirecto de los residuales, el cual nos
    # servira para establecer un relacion entre la observacion i y su residual.
    # Lo que se hace aqui es crear una lista z con los valores de data ordenados
    # de acuerdo a su respectivo valor en r
    z = [data[i] for i in numpy.argsort(r)]
    mean_z = numpy.mean(z)
    # el promedio de los primero n - k elementos
    mean_z_minus_k = numpy.mean(z[0:-k])
    
    # obtenemos los valores de la formula descrita por la prueba estadistica Ek
    numerator = math.fsum([(e - mean_z_minus_k) ** 2 for e in z[:-k]])
    denominator = math.fsum([(e - mean_z) ** 2 for e in z])

    return numerator / denominator


def experiment(n, k):
    """ Realiza un experimento, obteniendo n observaciones, y aplica una
    prueba estadistica TN16 para estos datos
    """

    data = [random.normalvariate(0, 1) for i in range(n)]
    return ek(n, k, data)


# n = 10000
# for j in range(n):
#     c = [experiment(20, 1) for i in range(j)]
#     c.sort()
#     index = int(j * .01)
#     print j, " ", c[index]

import matplotlib.pyplot as plt

n = 10000
c = [experiment(20, 1) for i in range(n)]
c.sort()
# plt.plot([v for v in c])
# plt.show()
index = int(n * .01)
print(c[index])


# i = 0
# for v in c:
#     print i ," ", c[i]
#     i += 1
