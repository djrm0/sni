# -*- coding: utf-8
#
# Author: Daniel J. Ramirez <danielramirezz123@gmail.com>
#

import math
import json
import subprocess
import uuid
import os.path
import lzma
import multiprocessing
from multiprocessing import Queue, Process


class TableUtils:
    """ Utilidades para manipular y crear Tablas de valores criticos para
    la prueba estadistica TN16, esta clase tambien provee metodos para la
    obtencion de datos de las tablas.

    """

    
    USE_COMPRESSION = True
    
    DIR = ''

    # templates para generar metadatos
    METADATA = {
        'observations': 0
        , 'seeds': []
        , 'values_used': []
        , 'cells': {}
        , 'ok': False
    }
    CELL = {
        'n': 0
        , 'k': 0
        , 'files': []
        , 'completed': False
    }
    FILE = {
        'name': ''
        , 'compressed': False
        , 'seed_index': 0
        , 'completed': False
    }

    __meta = None

    __CORES = multiprocessing.cpu_count()
    
    
    def __init__(self, table_dir):
        self.DIR = table_dir

        if not os.path.exists(self.DIR):
            os.makedirs(self.DIR)

        if os.path.isfile(self.DIR + 'metadata.json'):
            with open(self.DIR + 'metadata.json') as f:
                self.__meta = json.load(f);


    def create_table_structure(self, observations, seeds):
        """ Retorna una cadena formateada en json con los metadatos para una
        nueva tabla, las funciones para generar muestreos para la tabla
        obedeceran la estructura impuesta por el archivo que aqui se genera
        
        """

        if self.__meta:
            return
    
        # llenamos la informacion necesaria para los metadatos
        self.__meta = self.METADATA.copy()
        self.__meta['observations'] = observations
        self.__meta['seeds'] = seeds
        self.__meta['values_used'] = [0 for i in seeds]
        self.__meta['ok'] = True
        self.commit_metadata()


    def __cell_struct(self, n, k):
        """ Retorna una nueva estructura de celda """
        
        seeds = self.__meta['seeds']
        cell_dir = "n_%d/k_%d/" % (n, k)
        if not os.path.exists(self.DIR + cell_dir):
            os.makedirs(self.DIR + cell_dir)
        files = []
        # generamos los archivos
        for i in range(len(seeds)):
            files.append(self.__file_struct(i, cell_dir))
        cell = self.CELL.copy()
        cell['n'] = n;
        cell['k'] = k;
        cell['files'] = files
        
        return cell


    def __file_struct(self, seed_index, cell_dir):
        t_file = self.FILE.copy()
        t_file['name'] = cell_dir + str(uuid.uuid4()) + '.cr'
        if self.USE_COMPRESSION:
            t_file['name'] += '.lzma'
            t_file['compressed'] = True
        t_file['seed_index'] = seed_index
        return t_file


    def __sample_cell(self, cell):
        """ Genera los archivos de valores criticos para la celda 
        especificada

        """
        
        obs = self.__meta['observations']
        seeds = self.__meta['seeds']
        values_used = self.__meta['values_used']
        n = cell['n']
        k = cell['k']
        files = cell['files']
        cell_completed = True
        for c_file in files:
            if c_file['completed']:
                continue
            
            filename = c_file['name']
            seed = seeds[c_file['seed_index']]
            result = self.call_sampler(n, k, obs, seed, self.DIR + filename)
            if self.USE_COMPRESSION:
                c_file['compressed'] = True
            if result == 1:
                values_used[c_file['seed_index']] += 1
                c_file['completed'] = True
        cell_completed = cell_completed and c_file['completed']
        cell['completed'] = cell_completed


    def generate_cell(self, n, k):
        """ Genera los muestreos para la celda especificada, si la celda
        no se encuentra indexada en los metadatos, esta sera agregada """

        if k > int(n / 2):
            return

        cell = self.__meta['cells'].get('%d_%d' % (n, k))
        if not cell:
            cell = self.__cell_struct(n, k)
            self.__meta['cells']['%d_%d' % (n, k)] = cell
        if not cell['completed']:
            self.__sample_cell(cell)
            self.commit_metadata()


    def generate_cells(self, ns, max_outliers):
        """ Genera las celdas especificadas por el producto cruz entre ns y
        {0, ... , max_outliers}
        
        """

        for n in ns:
            for k in range(1, max_outliers + 1):
                self.generate_cell(n, k)


    @staticmethod
    def get_ns(increments):
        """ Parsea una cadena de incrementos, en un arreglo de valores, la
        sintaxis de la cadena de incrementos es la siguiente
               numero-incrementos otro_numero-incrementos ... numero_final
        por ejemplo si queremos los numero 3,4,5,6,7,8,9,10,12,14,16,18,20
        usamos la suiguiente cadena:
               3-1 10-2 20

        """
        
        p = increments.split(' ')
        intervals = [int(i.split('-')[0]) for i in p]
        increments = [int(i.split('-')[1]) for i in p[:-1]]

        ns = []  # este es el arreglo de los n's para los cuales se generaran
                 # valores criticos
        for i in range(len(intervals) - 1):
            for j in range(intervals[i], intervals[i + 1], increments[i]):
                ns.append(j)
        # este es el ultimo n no considerado en los ciclos anteriores
        ns.append(intervals[-1])
        
        return ns


    def generate_incomplete_cells(self):
        """ Genera los valores muestreos para aquellas celdas que han sido
        completadas.

        """

        for cell in self.__meta['cells'].values():
            if cell['completed']:
                continue
            self.generate_cell(cell['n'], cell['k'])


    def enqueue_cells(self, ns, max_outliers):
        """ Agrega las celdas especificadas a el indice, sin generar los
        muestreos

        """

        for n in ns:
            outliers = int(min(int(n / 2), max_outliers))
            for i in range(1, outliers + 1):
                if self.__meta['cells'].get('%d_%d' % (n, i)):
                    continue
                cell = self.__cell_struct(n, i)
                self.__meta['cells']['%d_%d' % (n, i)] = cell
        self.commit_metadata()
            


    def call_sampler(self, n, k, obs, seed, filename):
        """ Llama a el programa que realiza muestreos, usando los parametros
        especificados, el programa de muestreos colocara los datos generados
        en el archivo especificado en filename
        
        """

        a = subprocess.check_output(['./sampler', str(n), str(k), str(obs),
                                     str(seed), filename])
        # en caso de que la compression este habilitada se usara LZMA
        if self.USE_COMPRESSION:
            with lzma.open(filename, 'w') as f:
                f.write(a)
        else:
            with open(filename, 'w') as f:
                f.write(a.decode())
        return 1


    def add_seeds(self, new_seeds):
        """ Agrega las semillas especificadas a el indexado, dado que las
        nuevas semillas tienen que ser agregadas, esto modificara la propiedad
        de completez de todas las celdas, volviendolas celdas imcompletas,
        hasta que no se hayan simulado los valores criticos con las nuevas
        semillas.

        """

        seeds = self.__meta['seeds']
        seed_index = len(seeds)
        for new_seed in new_seeds:
            if new_seed in seeds:
                continue
            seeds.append(new_seed)
            self.__meta['values_used'].append(0)
            for cell in self.__meta['cells'].values():
                cell['completed'] = False
                cell_dir = 'n_%d/k_%d/'%(cell['n'], cell['k'])
                cell['files'].append(self.__file_struct(seed_index, cell_dir))
            seed_index += 1
        self.commit_metadata()


    @staticmethod
    def get_standard_deviation(values):
        """ Retorna la desviacion estandar de el conjunto dado """

        # calculamos la media
        mean = 0;
        for value in values:
            mean += value
        mean /= len(values)

        # se calcula la desviacion estandar
        sd = 0;
        for value in values:
            sd += (value - mean) ** 2
        sd = math.sqrt(sd / len(values))
        
        return sd

    
    @staticmethod
    def get_standard_error(values):
        """ Retorna el error estandar para la muestra especificada """
        
        sd = TableUtils.get_standard_deviation(values)
        se = sd / math.sqrt(len(values))
        return se


    def get_raw_criticals(self, n, k, alpha):
        """ Retorna todos los valores criticos para n experimentos con k
        outliers y nivel de confianza alpha

        """
        obs = self.__meta['observations']
        index = int(obs * alpha)

        cell = self.__meta['cells'].get("%d_%d" % (n, k))
        if not cell:
            return
        files = cell["files"]
        criticals = Queue()

        inc = max(1, int(len(files) / self.__CORES))

        p_files = [files[i:i+inc] for i in range(0, len(files), inc)]
        procs = []
        for file_list in p_files:
            p = Process(target=self.__get_criticals,
                        args=(file_list, criticals, index))
            p.start()
            procs.append(p)
        for proc in procs:
            proc.join()

        return [float(criticals.get()) for i in range(criticals.qsize())]
        


    def get_critical(self, *args):
        """ retorna """

        n = args[0]
        k = args[1]
        alpha = args[2]
        obs = self.__meta['observations']
        index = int(obs * alpha)
        if len(args) == 4:
            fake_obs = args[3]
            index = int((fake_obs / obs) * (obs * alpha))

        cell = self.__meta['cells'].get("%d_%d"%(n,k))
        if not cell:
            return
        files = cell["files"]
        criticals = Queue()

        inc = max(1, int(len(files) / self.__CORES))

        p_files = [files[i:i+inc] for i in range(0, len(files), inc)]
        procs = []
        for file_list in p_files:
            p = Process(target=self.__get_criticals,
                        args=(file_list, criticals, index))
            p.start()
            procs.append(p)
        for proc in procs:
            proc.join()

        return [criticals.get() for i in range(criticals.qsize())]

    
    def __get_criticals(self, files, criticals, index):
        for f in files:
            filename = self.DIR + f['name']
            if f['compressed']:
                with lzma.open(filename) as ff:
                    c = ff.read().decode().split(' ')
                    c.pop()
                    criticals.put(c[index])
                    
        
    def commit_metadata(self):
        """ Guarda los metadatos almacenados a el archivo metadata.json 
        si los datos no son guardados este modulo no tomara en cuenta los
        cambios realizados

        """
        
        with open(self.DIR + 'metadata.json', 'w') as outfile:
            json.dump(self.__meta, outfile)
