/** Programa que realiza pruebas de convergencia para la prueba TN16
 */

#include "TN16.cpp"
#include <vector>
#include <iostream>
#include <fstream>
#include <chrono>
#include <thread>
#include <string>
#include <set>

// #define MAX_SIM_SIZE 1000000
#define MAX_SIM_SIZE 10000


/** Genera un archivo que contiene una lista de pares ordenados que muestran
 *  (cantidad_de_simulaciones, valor critico de la simulacion) */
void plot(int observations, int outliers, double alpha) {
  std::ofstream dataFile;  // archivo de salida
  std::string fileName = "N" + std::to_string(observations) 
    + "_K" + std::to_string(outliers) + "_a" + std::to_string(alpha) + ".plt";
  dataFile.open(fileName);

  // agregamos formateo para gnuplot
  dataFile << "set encoding utf8\n";
  dataFile << "set ylabel \"Valor Critico\"\n";
  dataFile << "set xlabel \"Tamaño de la simulacion\"\n";
  // dataFile << "set label \"{/Symbol a}=" << alpha << ", N=" << observations
  //          << ", k=" << outliers
  //          << "\"  \n";
  dataFile << "plot \"-\" title \"TN16 {/Symbol a}=" << alpha 
           << ", N=" << observations << ", k=" << outliers
           << "\"\n";

  unsigned seed1 = std::chrono::system_clock::now().time_since_epoch().count();
  TN16 tn16 (seed1);

  int add = 100;
  int simSize = 10;
  // aqui almacenaremos los valores criticos de las pruebas
  std::vector<double> critical (MAX_SIM_SIZE, 0.0);
  while (simSize <= MAX_SIM_SIZE) {
    for (int i = 0; i < simSize; i++) {
      critical[i] = tn16.d_experiment(observations, outliers);
    }
    // ordenamos el fragmento de la lista que contiene valores
    std::sort(critical.begin(), critical.begin() + simSize - 1);
    int index = (int)((double)simSize * alpha);
    
    dataFile << simSize << " " << critical[index] << "\n";
    if (simSize % add == 0) {add *= 10;}
    simSize += add / 100;
  }
  dataFile << "end\n" << "pause -1\n";
  dataFile.close();
}


/** Este programa realiza pruebas de convergencia para la prueba estadistica
 *  TN16 (Ek) tietjen-moore */
int main(int argc, char **argv) {
  // unsigned seed1 = std::chrono::system_clock::now().time_since_epoch().count();
  // TN16 tn16 (seed1);

  unsigned int cores = std::thread::hardware_concurrency();
  std::cout << "cores: " << cores << std::endl;


  // int ns = [];
  // int ks = [];

  int outliers = 1;

  // plot(20, 1, .01);
  
  std::thread t1(plot, 20, 1, .01);
  std::thread t2(plot, 20, 2, .01);
  t1.join();
  t2.join();
  

  return 1;
}
