from TableUtils import TableUtils

t = TableUtils('results/testing_tables/table4/')

obs = int(100)
seeds = [0x4cc1d30f]
other_seeds = [0x34500]
t.create_table_structure(obs, seeds);

t.generate_cell(10, 2)

t.add_seeds(other_seeds)

t.generate_cell(10, 2)

# t = TableUtils('table/')

# increments = '3-1 20'
# max_outliers = 10

# t.table()
# #t.generate_cell(51, 1)
# b = t.get_critical(15, 2, .01);
# print(b)


# t = TableUtils('table_err_3m/')

# obs = int(3e6)
# seeds = [0x4cc1d30f, 0x999, 0x34500, 0xa7bdf825
# increments = '3-1 20'
# max_outliers = 10
# t.create_table_structure(obs, increments, seeds, max_outliers);

# t.table()
# #t.generate_cell(51, 1)
# b = t.get_critical(15, 2, .01);
# print(b)
