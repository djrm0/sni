#
#
#

from TableUtils import TableUtils
from decimal import *


t = TableUtils('results/tables/table2/')

criticals = t.get_raw_criticals(100, 10, .01)
err = TableUtils.get_standard_error(criticals)
print(Decimal(err).quantize(Decimal('1.000000')))

err_file = open('errores.csv', 'w')
err_file.write('N / k,')
for i in range(1, 21):
    err_file.write('K = %d,' % i)
err_file.write('\n')

ns = TableUtils.get_ns('3-1 50-5 80-10 100')
for n in ns:
    err_file.write('%d,' % n)
    for k in range(1, int(min((n / 2) + 1, 20))):
        print('N %d - K %d' % (n, k))
        criticals = t.get_raw_criticals(n, k, .01)
        err = TableUtils.get_standard_error(criticals)
        err = Decimal(err).quantize(Decimal('1.000000'))
        err_file.write('%f,' % err)
    err_file.write('\n')

exit()


# 3 millones de observaciones
obs = int(3e6)

seeds = [
    [0x34500, 0x456, 0x67800, 0x567 ]
    , [ 0x680, 0x888, 0x1111, 0x341 ]
    , [ 0x999, 0xaaa, 0x1, 0xa ]
    , [ 0x3, 0xa, 0xb, 0xc ]
    , [ 0x01, 0x2, 0x1, 0x1 ]
    , [ 0xea857ccd, 0x4cc1d30f, 0x8891a8a1, 0xa6b7aadb ]
    , [ 0x512c0c03, 0x5a9ad5d9, 0x885d05f5, 0x4e20cd47 ]
    , [ 0x76, 0x111, 0xaaa, 0x885d05f5 ]
    , [ 0xeee, 0x3000, 0x885d05f5, 0xababa ]
    , [ 0x4ebc98, 0x1, 0xffff, 0x2da87693 ]
    , [ 0x95f24dab, 0x0b685215, 0xe76ccae7, 0xaf3ec239 ]
    , [ 0x512c0c03, 0xea857ccd, 0x4cc1d30f, 0x8891a8a1 ]
    , [ 0xfffabc, 0xbcdfff, 0xfffdef, 0xcde000 ]
    , [ 0x345, 0x456, 0x678, 0x56dd7 ]
    , [ 0xabc, 0xffbcd, 0xdefff, 0xcde ]
    , [ 0x95f24dab, 0x24a590ad, 0xc1de75b7, 0x8121da71 ]
    , [ 0x512c0c03, 0x0b685215, 0x69e4b5ef, 0x8858a9c9 ]
    , [ 0x8b823ecb, 0xea857ccd, 0xe76ccae7, 0xbf456141 ]
    , [ 0x2da87693, 0x885d05f5, 0x4cc1d30f, 0x8891a8a1 ]
    , [ 0x715fad23, 0xa7bdf825, 0xffdc8a9f, 0x5a9ad5d9 ]
    , [ 0x03e5, 0x0223, 0x0277, 0x2d7 ]
]
seeds = [s[0] for s in seeds]

t.create_table_structure(obs, seeds);
ns = TableUtils.get_ns('3-1 50-5 80-10 100')
t.enqueue_cells(ns, 50)
t.generate_incomplete_cells()
#print(t.get_critical(10, 2, .1))
# t.generate_cell(10, 2)
# t.add_seeds(other_seeds)
# t.generate_cell(10, 2)
