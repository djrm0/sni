#include <vector>
#include <iostream>
// #include <chrono>
// #include <ctime>
#include <fstream>

#include "TN16.hpp"


/** genera un archivo con un muestreo de valores criticos para la prueba
 *  estadistica tn16, este programa requiere al menos 3 argumentos, los cuales
 *  son <cantidad de datos> <numero de outliers> <cantidad de observaciones>
 *  el cuarto argumento es opcional e indica el directorio donde se colocaran
 *  los resultados, si este parametro no es especificado los archivos se
 *  colocaran en el directorio de donde se ejecuta el programa */
int main(int argc, char **argv) {
  using std::cout;
  using std::endl;
  // using std::chrono::milliseconds;
  // using std::chrono::system_clock;
  // using std::chrono::duration_cast;

  if (argc < 6) {
    cout << "uso: sampler <n> <k> <obs> <seed1> <seed2>" << endl;
    return 1;
  }


  const unsigned int CORES = std::thread::hardware_concurrency();
  const unsigned int N = atoi(argv[1]);
  const unsigned int K = atoi(argv[2]);
  const unsigned int OBS = atoi(argv[3]);
  const unsigned int SEED = atoi(argv[4]);
  std::string filename = argv[5];

  
  TN16<float> tn16 (SEED, CORES);
  
  std::vector<float>* criticals = tn16.sample(N, K, OBS);
  std::sort(criticals->begin(), criticals->end());

  for (int i = 0; i < OBS; i++) {
    cout << criticals->at(i) << " ";
  }
  delete criticals;
  
  return 0;
}
