#include <vector>
#include <iostream>
#include <random>
#include <numeric>
#include <chrono>

#include "TN16.hpp"


/** Pruebas para la clase TN16, referirse a este programa para comprender como
 *  funciona la clase TN16 */
int main(int argc, char **argv) {
  unsigned randomSeed =
    std::chrono::system_clock::now().time_since_epoch().count();

  /** creamos una instancia dada una semilla aleatoria, que usara dobles
   *  para representar los numeros */
  TN16<double> tn16 (randomSeed);

  /** creamos una instancia dada una semilla aleatoria, que usa 2 hilos,
   *  que usa flotantes para representar los numeros */
  TN16<float> tn16_threaded (randomSeed, 2);

  /** creamos una instancia dada una semilla aleatoria, que usa 2 hilos y
   *  espera 10 millisegundos */
  TN16<double> tn16_threaded_sleepy (randomSeed, 2, 10);

  /** Asi reasignamos la semilla para una instancia de TN16 */
  tn16_threaded_sleepy.seed(19650218);


  /** cuando se usa el metodo d_tn16, tenemos que proveer un arreglo de
   *  datos que son obtenidos de una distribucion normal estandar este metodo
   *  se usa en caso de que se pretenda usar un generador de numeros 
   *  aleatorios distinto al que ofrece la clase TN16 */
  std::vector<double> data(20, 0.0);
  std::vector<float> f_data(20, 0.0);
  
  /**  este sera nuestro generador aleatorio */
  std::normal_distribution<double> d_distrib =
    std::normal_distribution<double>(0.0, 1.0);
  std::mt19937_64 g = std::mt19937_64(randomSeed);
  for (int i = 0; i < 20; i++) {
    data[i] = d_distrib(g);
    f_data[i] = (float)d_distrib(g);
  } // fin del generador de numeros
  
  double c_from_specified_data = tn16.tn16(20, 2, &data);

  
  /** Usando la instancia multihilo de tn16 no obtendremos un beneficio en
   *  este metodo puesto que un experimento no es una operacion con gran 
   *  catidad de datos, es decir se operara sobre arreglos de longitud n 
   *  que a lo mas sera 5000 */
  float c_from_specified_data2 = tn16_threaded.tn16(20, 2, &f_data);
  

  /** cuando se usa el metodo d_experiment, se genera el arreglo de n
   *  datos usando la implementacion de distribucion normal y el generador
   *  de numeros aleatorios que estan definidos en TN16, refierase a TN16.hpp
   *  para conocer los detalles de implemtacion, los unico que hace
   *  d_experiment es generar el arreglo de n datos y llamar a el metodo
   *  tn16  */
  
  /** realizamos un experimento Ek con n = 20 y k = 2, c sera el valor 
   *  critico de la prueba. El prefijo d indica que el valor retornado sera
   *  un doble */
  double c = tn16.experiment(20, 2);

  /** realizamos un experimento Ek con n = 40 y k = 2, c sera el valor critico
   *  del experimento, el uso de multihilos es irrelevante en este metodo
   *  por las mismas razones que el metodo tn16 */
  float c2 = tn16_threaded.experiment(40, 2);

  /** usando la instancia de TN16 multihilo y que tiene tiempo de descanso,
   *  experiment y d_tn16 no presentaran cambios a la version sin tiempo
   *  de descanso, esto es porque el metodo tn16 es considerado una operacion
   *  elemental.*/
  double c3 = tn16_threaded_sleepy.experiment(40, 2);

  /** obtenemos un muestreo de 100000 valores criticos de la prueba tn16
   *  con n = 20 y k = 2 */
  std::vector<double>* sample = tn16.sample(20, 2, 100000);

  /** obtenemos un muestreo de 100000 valores criticos de la prueba tn16
   *  con n = 20 y k = 2, en este caso el muestreo se hara de forma
   *  concurrente */
  std::vector<float>* sample_threaded = tn16_threaded.sample(20, 2, 100000);

  delete sample;
  delete sample_threaded;
}
