#-*- coding utf-8
#
# Author: Daniel J. Ramirez <danielramirezz123@gmail.com>
#
# Use este programa como referencia y guia para usar la clase TableUtils
#

# importamos la clase TableUtils
from TableUtils import TableUtils


# en el proceso de construccion se genera un directorio cuyo nombre es el
# parametro de construccion, en este caso sera 'mi_tabla', si el directorio
# ya existe entonces la clase buscara dentro de el un archivo llamado
# metadata.json, y obtendra de ahi los datos de la tabla
table = TableUtils('mi_tabla/')

# si aun no tienes un archivo metadata.json usa los siguiente
observations = 100
seeds = [23412, 1237896]
table.create_table_structure(observations, seeds)
# esto generara la informacion necesaria para poder trabajar con
# TableUtils

# si queremos generar una celda hacemos
n = 10  # cantidad de datos
k = 3   # cantidad de 'outliers'
table.generate_cell(n, k)
# esto puede tardar un tiempo, dependiendo de la cantidad de
# observaciones, el valor de n, y la cantidad de semillas

# si queremos generar multiples celdas usamos
ns = [3, 4, 5]
max_outliers = 2
table.generate_cells(ns, 2)
# esto generara las celdas (3,1), (4,1), (4,2), (5,1), (5,2)

# si queremos usar intervalos mas dificiles podemos usar la funcion
ns = TableUtils.get_ns('3-10 10-2 20')
# esto generara un arreglo ns = [3,4,5,6,7,8,9,10,12,14,16,18,20]
# el cual podemos usar en la siguiente funcion
table.generate_cells(ns, 10)
# este metodo puede tardar mucho tiempo

# si queremos colocar celdas en el indice sin generar sus muestreos,
# es decir ponerlas en cola. usamos
ns = TableUtils.get_ns('10-1 20')
table.enqueue_cells(ns, 10)
# notese que anteriormente ya se habian simulado los muestreos para
# los n's [10, 12, 14, 16, 18, 20], dado que ahora ns = [10,11,12,...,20]
# se agregaran a la cola las celdas con n's [11,13,15,17,19]

# para generar las celdas en cola usamos
table.generate_incomplete_cells()

# podemos agregar mas semillas en cualquier momento usando
table.add_seeds([1929073, 238793])
# esto hara que las celdas que ya estaban completadas previamente ya no
# lo esten, hasta que se hallan simulado las nuevas semillas

# para simular las celdas no completadas usamos
table.generate_incomplete_cells()

# para obtener el valor critico de una celda usamos
n = 10
k = 2
alpha = .01
critical = table.get_critical(n, k, alpha)
# este metdo puede tardar un poco si la compresion esta habilitada,
# sin embargo se recomienda ampliamente usar la compresion dado que el
# tamaño de los archivo no comprimidos es muy grande, y la compression
# reduce en un factor de 100 el tamaño de los archivos.
