#include <vector>
#include <array>
#include <iostream>
#include <chrono>
#include <ctime>
#include <fstream>

#include "TN16.cpp"


/** El objetivo de este programa es generar una serie de valores criticos
 *  para que se analiza el error numerico que estos poseen.
 *  genera un archivo con un muestreo de valores criticos para la prueba
 *  estadistica tn16, este programa requiere al menos 4 argumentos, los cuales
 *  son <cantidad de datos> <numero de outliers> <cantidad de observaciones>
 *  <cantidad de muestras que se generaran>,
 *  el quinto argumento es opcional e indica el directorio donde se colocaran
 *  los resultados, si este parametro no es especificado los archivos se
 *  colocaran en el directorio de donde se ejecuta el programa */
int main(int argc, char **argv) {
  using std::cout;
  using std::endl;
  using std::chrono::milliseconds;
  using std::chrono::system_clock;
  using std::chrono::duration_cast;

  if (argc < 5) {
    cout << "uso sampler <n> <k> <alpha> <obs> <count>[output dir]" << endl;
    return -1;
  }

  const std::array<double, 6> ALPHAS = {.01, .025, .05, .1, .2, .3};
  // const double ALPHAS[] = {.01, .025, .05, .1, .2, .3};

  const unsigned int CORES = std::thread::hardware_concurrency();
  const unsigned int N = atoi(argv[1]);
  const unsigned int K = atoi(argv[2]);
  const unsigned int OBS = atoi(argv[3]);
  const unsigned int COUNT = atoi(argv[4]);
  std::string output_dir = "";

  if (argc > 5) {
    output_dir = argv[5];
    if (output_dir.back() != '/') output_dir += '/';
  }

  // semillas de prueba
  unsigned seed1 = system_clock::now().time_since_epoch().count();
  unsigned seeds[] = {
    0x34500, 0x456, 0x67800, 0x567, 0x680, 0x999, 0xaaa, 0x1, 0xa,
    0x3, 0xa, 0xb, 0xc, 0x01, 0x2, 0x1,
    0xea857ccd, 0x4cc1d30f, 0x8891a8a1, 0xa6b7aadb,
    0x512c0c03, 0x5a9ad5d9, 0x885d05f5, 0x4e20cd47,
    0x76, 0x111, 0xaaa, 0x885d05f5,
    0xeee, 0x3000, 0x885d05f5, 0xababa
  };
  seed1 = seeds[rand() % 32];
  TN16 tn16 (seed1, CORES);


  auto start = system_clock::now();  // medicion de tiempo
  
  std::ofstream out_file;
  system_clock::time_point now = system_clock::now();
  std::time_t tt = system_clock::to_time_t(now);
  std::string timestamp = ctime(&tt);
  std::replace(timestamp.begin(), timestamp.end(), ' ', '-');
  timestamp.pop_back();
  std::string file_name = "N-" + std::to_string(N) + "_K-" +
    std::to_string(K) + "_OBS-" + std::to_string(OBS) + "_"
    + timestamp
    + ".txt";
  out_file.open(output_dir + file_name);
  

  for (int i = 0; i < COUNT; i++) {
    std::vector<double>* criticals = tn16.d_sample(N, K, OBS);
    std::sort(criticals->begin(), criticals->end());

    for (int i = 0; i < ALPHAS.size(); i++) {
      int index = (int)((double)OBS * ALPHAS[i]);
      double critical = criticals->at(index);
      out_file << critical << " ";
    }
    out_file << endl;
    delete criticals;
  }

  auto duration = duration_cast<milliseconds>(system_clock::now() - start);
  cout << "time: " << duration.count() << "ms" << endl;

  out_file.close();
}
