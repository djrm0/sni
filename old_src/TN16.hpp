/**
 * Author: Daniel J. Ramirez <danielramirezz123@gmail.com>
 */
#ifndef TN16_H
#define TN16_H

#include <random>
#include <chrono>
#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <numeric>
#include <future>
#include <thread>

#include <stdlib.h>
#include <math.h>


/** Representa una prueba estadistica tietjen-moore Ek, esta clase contiene todos
 *  los metodos necesarios para generar valores criticos para esta prueba */
template <typename T>
class TN16 {
public:
  template <typename Sseq>
  TN16(Sseq& seed1, Sseq& seed2, unsigned threads, unsigned sleep);
  template <typename Sseq>
  TN16(Sseq& seed1, Sseq& seed2, unsigned threads);
  template <typename Sseq>
  TN16(Sseq& seed1, Sseq& seed2);
  ~TN16();


  /** Cambia la semilla para esta prueba estadistica, por el valor dado */
  template <typename Sseq>
  void seed(Sseq &seed1, Sseq &seed2);


  /** Realiza una prueba de estadistica Lk de tietjen and moore para k datos
   *  anomalos y n observaciones, sobre el conjunto de datos x de los cuales se sospecha que los k valores mas pequeños son sospechoso si lower es true, o si los k valores mas grandes son sospechoso si lower es false.
   *  Retorna el valor critico obtenido */
  T L_k(unsigned n, unsigned k, std::vector<T>*y, bool lower);

  /** Realiza una prueba de estadistica Ek de tietjen and moore para k datos
   *  anomalos y n observaciones, sobre el conjunto de datos x.
   *  Retorna el valor critico obtenido */
  T E_k(unsigned n, unsigned k, std::vector<T>*x);

  /** Realiza una prueba de estadistica Ek de tietjen and moore para k datos
   *  anomalos y n observaciones, sobre el conjunto de datos x.
   *  Retorna el valor critico obtenido */
  T tn16(unsigned n, unsigned k, std::vector<T>*x);


  /** Funcion que realiza un experimento aleatorio usando una distribucion
   *  Normal, generando valores aleatorios con el algoritmo mersenne twister,
   *  esta funcion retorna el valor critico del experimento */
  T experiment(unsigned n, unsigned k);

  /** Funcion que realiza un experimento aleatorio usando una distribucion
   *  Normal, generando valores aleatorios con el algoritmo mersenne twister,
   *  esta funcion retorna el valor critico del experimento */
  T L_k_experiment(unsigned n, unsigned k, bool lower);

  /** Funcion que realiza un experimento aleatorio usando una distribucion
   *  Normal, generando valores aleatorios con el algoritmo mersenne twister,
   *  esta funcion retorna el valor critico del experimento */
  T E_k_experiment(unsigned n, unsigned k);


  /** Genera un muestreo de c valores criticos, para el n y k especificados */
  std::vector<T>* L_k_sample(unsigned n, unsigned k, unsigned observations, bool lower);

  /** Genera un muestreo de c valores criticos, para el n y k especificados */
  std::vector<T>* E_k_sample(unsigned n, unsigned k, unsigned observations);

  /** Genera un muestreo de c valores criticos, para el n y k especificados */
  std::vector<T>* sample(unsigned n, unsigned k, unsigned observations);


  /** Funcion de donde se obtienen todos los valores pseudoaleatorios */
  T nextRandom();


  /** Metodo de marsaglia para retornar variables normales estandar
    * media = 0, desviacion estandar = 1
    */
  T marsaglia();


private:
  std::uniform_real_distribution<T> distrib;
  /** Generadores de numeros aleatorios (Mersenne-twister), son necesarios
    * los dos para el metodo de Marsaglia */
  std::mt19937_64 g1;
  std::mt19937_64 g2;

  unsigned max_threads;
  unsigned sleep = 0;  // el tiempo de espera en millisegundos

  bool return_first = true;  // usado para el metodo de marsaglia


  /** esta funcion genera valores criticos usando como parametros de la prueba
   *  a n y k, llenando las posiciones de i a j en el arreglo criticals */
  void experiments(unsigned n, unsigned k, std::vector<T>*criticals,
                   unsigned i, unsigned j);

   /** esta funcion genera valores criticos usando como parametros de la prueba L_k
    *  a n y k, llenando las posiciones de i a j en el arreglo criticals */
   void L_k_experiments(unsigned n, unsigned k, std::vector<T>*criticals,
                        unsigned i, unsigned j, bool lower);


  /** esta funcion genera valores criticos usando como parametros de la prueba
   *  a n y k, llenando las posiciones de i a j en el arreglo criticals */
  void E_k_experiments(unsigned n, unsigned k, std::vector<T>*criticals,
                       unsigned i, unsigned j);

  template <class Iter>
  T mean(Iter first, Iter last, typename Iter::iterator_category *p=0);
};



/*****************************************************************************/
/* IMPLEMENTACION */
/*****************************************************************************/



template <typename T>
template <typename Sseq>
TN16<T>::
TN16(Sseq& seed1, Sseq& seed2, unsigned threads, unsigned sleep) {
  distrib = std::uniform_real_distribution<T>(0.0, 1.0);
  g1 = std::mt19937_64(seed1);
  g2 = std::mt19937_64(seed2);
  max_threads = threads;
  sleep = sleep;
}


template<typename T>
template <typename Sseq>
TN16<T>::
TN16(Sseq& seed1, Sseq& seed2, unsigned threads) : TN16(seed1, seed2, threads, 0) {}


template<typename T>
template <typename Sseq>
TN16<T>::
TN16(Sseq& seed1, Sseq& seed2) : TN16(seed1, seed2, 1, 0) {}


template<typename T>
TN16<T>::
~TN16() {}


template<typename T>
template <typename Sseq>
void TN16<T>::
seed(Sseq& s1, Sseq& s2) {
  g1.seed(s1);
  g2.seed(s2);
}


bool descendent (int i,int j) { return (i > j); }


template<typename T>
T TN16<T>::
L_k(unsigned int n, unsigned int k, std::vector<T>*y, bool lower) {
  // ordenamos el arreglos en forma ascendente
  if (lower) {
    std::sort(y->begin(), y->end(), descendent);
  }
  else {
    std::sort(y->begin(), y->end());
  }

  T meanY = mean(y->begin(), y->end());
  // // media de los primeros n - k elementos de r
  T meanYminusK = mean(y->begin(), y->end() - k);

  // obtenemos el numerador
  T numerator = 0.0;
  for (int i = 0; i < n - k; i++) {
    numerator += pow(y->at(i) - meanYminusK, 2);
  }
  // obtenemos el denominador
  T denominator = 0.0;
  for (int i = 0; i < n; i++) {
    denominator += pow(y->at(i) - meanY, 2);
  }

  return numerator / denominator;
}


template<typename T>
T TN16<T>::
E_k(unsigned int n, unsigned int k, std::vector<T>*x) {
  T meanX = mean(x->begin(), x->end());
  // llenamos el arreglo de valores r (residuales absolutos)
  std::vector<T> r (n, 0.0);
  for (int i = 0; i < n; i++) {
    r[i] = fabs(x->at(i) - meanX);
  }

  // debemos ordenar los valores de r en forma indirecta dado que necesitamos
  // ordenar el arreglo con respecto a los valores en r, x ordenado en base a
  // los indices obtenidos aqui forman el arreglo z descrito en tietjen-moore
  std::vector<std::size_t> indices(n);
  std::iota(indices.begin(), indices.end(), 0);
  std::sort(indices.begin(), indices.end(),
            [&r](std::size_t left, std::size_t right) {
              return r[left] < r[right];
            });

  // reordenamiento de z
  std::vector<T> z (n);
  for (int i = 0; i < n; i++) {
    z[i] = x->at(indices[i]);
    // std::cout << z[i] << std::endl;
  }

  // std::sort(r.begin(), r.end()); // r debe estar ordenado ascendentemente
  T meanZ = mean(z.begin(), z.end());
  // // media de los primeros n - k elementos de r
  T meanZminusK = mean(z.begin(), z.end() - k);

  // obtenemos el numerador
  T numerator = 0.0;
  for (int i = 0; i < n - k; i++) {
    numerator += pow(z[i] - meanZminusK, 2);
  }
  // obtenemos el denominador
  T denominator = 0.0;
  for (int i = 0; i < n; i++) {
    denominator += pow(z[i] - meanZ, 2);
  }

  return numerator / denominator;
}


// DEPRECATED
template<typename T>
T TN16<T>::
tn16(unsigned int n, unsigned int k, std::vector<T>*x) {
  T meanX = mean(x->begin(), x->end());
  // llenamos el arreglo de valores r (residuales absolutos)
  std::vector<T> r (n, 0.0);
  for (int i = 0; i < n; i++) {
    r[i] = fabs(x->at(i) - meanX);
  }

  // debemos ordenar los valores de r en forma indirecta dado que necesitamos
  // ordenar el arreglo con respecto a los valores en r, x ordenado en base a
  // los indices obtenidos aqui forman el arreglo z descrito en tietjen-moore
  std::vector<std::size_t> indices(n);
  std::iota(indices.begin(), indices.end(), 0);
  std::sort(indices.begin(), indices.end(),
            [&r](std::size_t left, std::size_t right) {
              return r[left] < r[right];
            });

  // reordenamiento de z
  std::vector<T> z (n);
  for (int i = 0; i < n; i++) {
    z[i] = x->at(indices[i]);
    // std::cout << z[i] << std::endl;
  }

  // std::sort(r.begin(), r.end()); // r debe estar ordenado ascendentemente
  T meanZ = mean(z.begin(), z.end());
  // // media de los primeros n - k elementos de r
  T meanZminusK = mean(z.begin(), z.end() - k);

  // obtenemos el numerador
  T numerator = 0.0;
  for (int i = 0; i < n - k; i++) {
    numerator += pow(z[i] - meanZminusK, 2);
  }
  // obtenemos el denominador
  T denominator = 0.0;
  for (int i = 0; i < n; i++) {
    denominator += pow(z[i] - meanZ, 2);
  }

  return numerator / denominator;
}


// DEPRECATED
template<typename T>
T TN16<T>::
experiment(unsigned int n, unsigned int k) {
  // llenamos el arreglo con n valores aleatorios obtenidos de nuestra
  // distribucion, y nuestro generador de numeros aleatorios, esta sera
  // la poblacion de que evaluaremos en la prueba estadistica.
  std::vector<T> data (n, 0.0);
  for (int i = 0; i < n; i++) {
    data[i] = nextRandom();
  }
  return tn16(n, k, &data);
}

template<typename T>
T TN16<T>::
L_k_experiment(unsigned int n, unsigned int k, bool lower) {
  // llenamos el arreglo con n valores aleatorios obtenidos de nuestra
  // distribucion, y nuestro generador de numeros aleatorios, esta sera
  // la poblacion de que evaluaremos en la prueba estadistica.
  std::vector<T> data (n, 0.0);
  for (int i = 0; i < n; i++) {
    data[i] = nextRandom();
  }
  return L_k(n, k, &data, lower);
}


template<typename T>
T TN16<T>::
E_k_experiment(unsigned int n, unsigned int k) {
  // llenamos el arreglo con n valores aleatorios obtenidos de nuestra
  // distribucion, y nuestro generador de numeros aleatorios, esta sera
  // la poblacion de que evaluaremos en la prueba estadistica.
  std::vector<T> data (n, 0.0);
  for (int i = 0; i < n; i++) {
    data[i] = nextRandom();
  }
  return E_k(n, k, &data);
}


// DEPRECATED
template<typename T>
std::vector<T>* TN16<T>::
sample(unsigned n, unsigned k, unsigned observations) {
  std::vector<T> *criticals = new std::vector<T>(observations, 0.0);
  // const size_t c_max_threads = max_threads;

  if (max_threads == 1) {
    experiments(n, k, criticals, 0, observations);

    return criticals;
  }

  // esto es en caso de que se use la implementacion multithread
  std::vector<std::thread> threads (max_threads);
  unsigned f = observations / max_threads;

  unsigned j = 0;
  unsigned l = f;
  for (int i = 0; i < max_threads; i++) {
    // al ultimo agremas el residuo de la division
    if (i == max_threads - 1) { l += observations % max_threads; }
    threads[i] = std::thread(&TN16<T>::experiments, this, n, k, criticals, j, l);
    j = l;
    l = j + f;
  }
  for (int i = 0; i < max_threads; i++) threads[i].join();

  return criticals;
}


template<typename T>
std::vector<T>* TN16<T>::
L_k_sample(unsigned n, unsigned k, unsigned observations, bool lower) {
  std::vector<T> *criticals = new std::vector<T>(observations, 0.0);
  // const size_t c_max_threads = max_threads;

  if (max_threads == 1) {
    L_k_experiments(n, k, criticals, 0, observations, lower);

    return criticals;
  }

  // esto es en caso de que se use la implementacion multithread
  std::vector<std::thread> threads (max_threads);
  unsigned f = observations / max_threads;

  unsigned j = 0;
  unsigned l = f;
  for (int i = 0; i < max_threads; i++) {
    // al ultimo agremas el residuo de la division
    if (i == max_threads - 1) { l += observations % max_threads; }
    threads[i] = std::thread(&TN16<T>::L_k_experiments, this, n, k, criticals, j, l, lower);
    j = l;
    l = j + f;
  }
  for (int i = 0; i < max_threads; i++) threads[i].join();

  return criticals;
}


template<typename T>
std::vector<T>* TN16<T>::
E_k_sample(unsigned n, unsigned k, unsigned observations) {
  std::vector<T> *criticals = new std::vector<T>(observations, 0.0);
  // const size_t c_max_threads = max_threads;

  if (max_threads == 1) {
    E_k_experiments(n, k, criticals, 0, observations);

    return criticals;
  }

  // esto es en caso de que se use la implementacion multithread
  std::vector<std::thread> threads (max_threads);
  unsigned f = observations / max_threads;

  unsigned j = 0;
  unsigned l = f;
  for (int i = 0; i < max_threads; i++) {
    // al ultimo agremas el residuo de la division
    if (i == max_threads - 1) { l += observations % max_threads; }
    threads[i] = std::thread(&TN16<T>::E_k_experiments, this, n, k, criticals, j, l);
    j = l;
    l = j + f;
  }
  for (int i = 0; i < max_threads; i++) threads[i].join();

  return criticals;
}


// DEPRECATED
template<typename T>
void TN16<T>::
experiments(unsigned n, unsigned k, std::vector<T>*criticals,
            unsigned i, unsigned j) {
  for (int l = i; l < j; l++) {
    criticals->at(l) = experiment(n, k);
  }
}


template<typename T>
void TN16<T>::
L_k_experiments(unsigned n, unsigned k, std::vector<T>*criticals,
            unsigned i, unsigned j, bool lower) {
  for (int l = i; l < j; l++) {
    criticals->at(l) = L_k_experiment(n, k, lower);
  }
}


template<typename T>
void TN16<T>::
E_k_experiments(unsigned n, unsigned k, std::vector<T>*criticals,
            unsigned i, unsigned j) {
  for (int l = i; l < j; l++) {
    criticals->at(l) = E_k_experiment(n, k);
  }
}


template <typename T>
template <class Iter>
T TN16<T>::
mean(Iter first, Iter last, typename Iter::iterator_category *p) {
  T mean = 0.0;
  int dataSize = 0;
  typedef typename std::vector<T>::iterator iterator_;
  for (iterator_ it = first; it != last; ++it) {
    mean += *it;
    dataSize++;
  }
  return mean / (T)dataSize;
}


/** Configure esta funcion para retornar los valores aleatorios */
template <typename T>
T TN16<T>::
nextRandom() {
    return marsaglia();
}

/** Metodo de marsaglia para retornar dos valores normales independientes */
template <typename T>
T TN16<T>::
marsaglia() {
    T u1, u2, w;
    do {
        u1 = distrib(g1) * 2 - 1;
        u2 = distrib(g2) * 2 - 1;
        w = u1 * u1 + u2 * u2;
    } while (w > 1);

    T y = sqrt((-2 * log(w)) / w);

    if (return_first) {
        return_first = false;
        return u1 * y;
    }
    else {
        return_first = true;
        return u2 * y;
    }
}

#endif
