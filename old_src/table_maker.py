import sys
from decimal import Decimal as D


def redondeo(media, error):
	pass


DIR = "results/new/"

alpha = str(sys.argv[1])
alpha = str(D(alpha).quantize(D('0.000000')))

table_C = ""   # valores criticos
table_SE = ""  # errores estandar
table_E = ""   # intervalo de confianza

for i in range(3, 15):
	dir = DIR + str(i) + "/"
	file_name_data = '_N_%d-alfa_%s.txt' % (i, alpha)
	c_file_name = "criticos" + file_name_data
	c_file = open(dir + c_file_name, 'r')
	c_line = c_file.readlines()[0].split(',')
	c_line.pop()

	se_file_name = "se" + file_name_data
	se_file = open(dir + 'error_estandar/' + se_file_name, 'r')
	se_line = se_file.readlines()[0].split(',')
	se_line.pop()

	e_file_name = "errores" + file_name_data
	e_file = open(dir + 'error/' + e_file_name, 'r')
	e_line = e_file.readlines()[0].split(',')
	e_line.pop()

	table_C += str(i) + ", "
	table_SE += str(i) + ", "
	table_E += str(i) + ", "

	for p in range(0, len(c_line)):
		if (p > 0):
			table_C += ", "
			table_SE += ", "
			table_E += ", "

		table_C += c_line[p].replace(' ', '')
		table_SE += se_line[p].replace(' ', '')
		table_E += e_line[p].replace(' ', '')
		p += 1
	table_C += '\n'
	table_SE += '\n'
	table_E += '\n'

	c_file.close()
	se_file.close()
	e_file.close()

criticals_file = open(DIR + "criticos_alfa_" + str(D(alpha).quantize(D('0.000'))) + '.csv', 'w')
criticals_file.write(table_C)

standard_error_file = open(DIR + "error_estandar_alfa_" + str(D(alpha).quantize(D('0.000'))) + '.csv', 'w')
standard_error_file.write(table_SE)

confidence_file = open(DIR + "confianza_alfa_" + str(D(alpha).quantize(D('0.000'))) + '.csv', 'w')
confidence_file.write(table_E)
