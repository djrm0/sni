# Este programa realiza llamadas sucesivas a el programa de generacion de valores criticos. Este programa existe para facilitar la generacion de valores para secuencias de tamaños de muestra definidas por cadenas en nomenclatura estadistica, este programa tambien realiza el 'loging' de los tiempos de ejecucion de cada llamada al programa de generacion de valores.
#
# author: Daniel J. Ramirez


import os
from timeit import default_timer as timer
import datetime
import sys
from utility import get_ns


if len(sys.argv) < 3:
    print("Uso: <ns_string> <directory> [program_path]")
    exit()

ns = None
try:
    ns = get_ns(sys.argv[1])
except:
    print("cadena de n's malformada")
    exit()
outdir = sys.argv[2]
if not outdir:
    print("Especifique un directorio")
    exit()

# crea directorio si este no existe, ademas crea un subdirectorio para almacenar logs
if not os.path.exists(outdir):
    os.makedirs(outdir)
logs_dir = outdir + '/logs' if outdir[-1] != '/' else outdir + 'logs'
if not os.path.exists(logs_dir):
    os.makedirs(logs_dir)
pg_path = sys.argv[3] if len(sys.argv) > 3 else './c'

# crea archivo de log
log_file_name = '%s' % datetime.datetime.now()
log_file_name = log_file_name.replace(' ', '__').replace(':', '-').replace('.', '_')

def write_log(str):
    log = open('%s/%s.log' % (logs_dir, log_file_name), 'a')
    log.write(str)
    log.close()

write_log('%s\n' % " ".join(sys.argv))

for i in ns:
    cmd = pg_path + " " + str(i) + " 100000 " + outdir
    log_entry = '%s\n' % ('-' * 70)
    log_entry += 'start: %s \n' % datetime.datetime.now()
    log_entry += 'command: %s \n' % cmd
    write_log(log_entry)
    print(log_entry)

    t = timer()
    os.system(cmd)
    elapsed_time = timer() - t

    log_entry = 'elapsed time: %s seconds\n\n' % elapsed_time
    write_log(log_entry)
    print(log_entry)
