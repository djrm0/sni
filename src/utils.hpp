#ifndef STATISTICAL_UTILS_H
#define STATISTICAL_UTILS_H

#include <vector>
#include <algorithm>
#include <iterator>
#include <numeric>

#include <stdlib.h>
#include <math.h>

#include "UnivariateStatisticalTest.hpp"


class CriticalComparatorResult: public ContaminedTestResult
{
  public:
    // asigna el valor critico objetivo.
    void set_target(double critical_target) {
      target = critical_target;
    };
    void process(unsigned n, unsigned k, std::vector<element_t> *x, double result_critical) {
      if (result_critical < target) { counter++; }
    };

    int get_counter() {
      return counter;
    }

  private:
    double target;
    int counter = 0;
};


template <class Iter>
double
mean(Iter first, Iter last) {
  double mean = 0.0;
  int dataSize = 0;
  typedef typename std::vector<element_t>::iterator iterator_;
  for (iterator_ it = first; it != last; ++it) {
    mean += *it;
    dataSize++;
  }
  return mean / (double)dataSize;
};


#endif
