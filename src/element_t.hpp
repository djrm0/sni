#ifndef ELEMENT_T_H
#define ELEMENT_T_H

struct element_t {
  bool contamined = false;
  double v;
  friend std::ostream &operator<<(std::ostream &os, const element_t& e);
  //
  // // conversion from A (constructor):
  // element_t (const A& x) {}
  // // conversion from A (assignment):
  // element_t& operator= (const double& x) {return *this;}
  // // conversion to A (type-cast operator)
  // operator double() {return double();}
  //
  // // equality comparison. doesn't modify object. therefore const.
  // bool operator==(const element_t& e) const
  // {
  //     return (value == e);
  element_t() { }
  element_t(const element_t& val) : v(val.v), contamined(val.contamined) { }
  element_t(const double val) : v(val) { }
  operator double() const { return v; }
  const element_t& operator=(const double val) { v = val; return *this; }
  const element_t& operator=(const element_t &val) { v = val.v; contamined = val.contamined; return *this; }
  const element_t& operator+=(const double &val) { v += val; return *this; }
  const element_t& operator+=(const element_t &val) { v += val.v; return *this; }
  const element_t& operator-=(const double &val) { v -= val; return *this; }

  // const element_t& operator-(double val) { v -= val; return *this; }


  // }
};
std::ostream &operator<<(std::ostream &os, const element_t& e) {
  return os << e.v;
}


// logical operators
inline bool operator==(const element_t& lhs, const element_t& rhs) {
  return lhs.v == rhs.v;
}
inline bool operator!=(const element_t& lhs, const element_t& rhs) {
  return !operator==(lhs,rhs);
}
inline bool operator< (const element_t& lhs, const element_t& rhs) {
  return lhs.v < rhs.v;
}
inline bool operator> (const element_t& lhs, const element_t& rhs){return  operator< (rhs,lhs);}
inline bool operator<=(const element_t& lhs, const element_t& rhs){return !operator> (lhs,rhs);}
inline bool operator>=(const element_t& lhs, const element_t& rhs){return !operator< (lhs,rhs);}


#endif
