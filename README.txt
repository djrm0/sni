
Calculo de valores criticos para la prueba estadistica tn16 tietjen-moore y conjunto de clases
para agregar funciones estadisticas extras.

Autor: Daniel J. Ramirez <djrmuv@gmail.com>
Todos los derechos reservados, 2015


Compilacion:
============
Antes que nada, es necesario tener los siguientes paquetes:
  bin-utils
  gcc-c++
  clang >= 3.4 o un compilador que soporte c++11
  python3

Todos los programas han sido probados unicamente en linux, sin embargo es
probable que no requieran modificaciones para correr en otras plataformas,
puesto que solo se usan librerias y modulos estandar de los lenguajes

Los programas escritos en python brindan facilidad de acceso a los programas
en c++, es decir, solo las partes criticas estan escritas en c++, por lo tanto
es necesario compilar los programas en c++ antes de usar los programas en
python.

estructura del codigo:
-----------------------

DIRECTORIO src:
  este directorio contiene las clases necesarias para la evaluacion de pruebas estadisticas univariadas, dentro de este directorio se encuentran los archivos:

   * element_t.hpp - especificacion de un contenedor usado por las implementaciones de pruebas univariadas
   * utils.hpp - brinda algunas funciones de utileria
   * UnivariateStatisticalTest.hpp - Clase abstracta que provee definiciones de metodos para pruebas de discordancia univariadas
   * StatisticalTestSampler.hpp - Evalua muestras usando implementaciones de pruebas de discordancia univariadas

   El subdirectorio 'tests', almacena implementaciones de pruebas de discordancia univariadas.


DIRECTORIO testing
  Contiene pruebas a los programas que estan en src



AGREGANDO NUEVAS PRUEBAS ESTADISTICAS:
--------------------------------------
Para agregar nuevas pruebas estadisticas es necesario extender la clase abstracta 'UnivariateStatisticalTest', ver 'src/UnivariateStatisticalTest.hpp' para mas informacion


COMPILACION DE PROGRAMAS
------------------------
Para compilar porgramas que hagan uso de 'StatisticalTestSampler' es necesario usar -lpthread.
